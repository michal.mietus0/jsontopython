<h1 align="center">JSON to Python </h1>

This extension allows you to perform `json.loads` on file through command.

# Usage

1. Install extension from [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=BringFuture.json-to-python)
2. Open file with JSON
3. Run command <kbd>json to python</kbd> and enjoy the converted object :)

# Showcase

![Showcase 1](assets/showcase.gif 'How to convert')
